package com.example.apirest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    //private RequestQueue queue;
    Button conectar, listar;
    ListView lista;
    EditText filter;
    private final String url="https://www.datos.gov.co/resource/ajwu-p33t.json";
    ArrayList<OperadorTuristico> cs = new ArrayList<OperadorTuristico>();

    OperadorTuristicoAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conectar = findViewById(R.id.btnconectar);
        lista = (ListView) findViewById(R.id.listaoperadores);
        listar = findViewById(R.id.btnlistado);
        filter = findViewById(R.id.editTextTextPersonName);
        List<OperadorTuristico> operadoresturisticos = this.cs;

        adaptador = new OperadorTuristicoAdapter(this,R.layout.item,operadoresturisticos);

        try {
            requestDatos();
        }catch(Exception e){
            System.out.println(e.toString());
        }

        conectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    requestDatos();
                }catch(Exception e){
                    System.out.println(e.toString());
                }
            }
        });

        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // aquí haz el filtro
                lista.setAdapter(adaptador);
            }
        });

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println(charSequence);

                ArrayList<OperadorTuristico> result = new ArrayList<OperadorTuristico>();
                for (OperadorTuristico item: cs) {
                    if (item.getNombremunicipio().contains(charSequence)) {
                        result.add(item);
                    }
                }

                adaptador = new OperadorTuristicoAdapter(getApplicationContext(),R.layout.item, result);
                lista.setAdapter(adaptador);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void requestDatos(){
        RequestQueue cola = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    //System.out.println(response.toString());
                    parserJson(response);
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error en la conexion", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                // headers.put("Content-Type", "application/json");
                //headers.put("X-Auth-Token", "df875ad8e5ac477cb91ca3687c170e6c");
                return headers;
            }
        };
        cola.add(jsonArrayRequest);
    }


    public void parserJson(JSONArray response){
        try {
            String cadena = "";
            for (int i = 0 ; i<response.length(); i++) {
                JSONObject com = response.getJSONObject(i);
                String nombresitio = com.getString("nombresitio");
                String tipo = com.getString("tipo");
                String nombremunicipio = com.getString("nombremunicipio");
                String descripcion = com.getString("descripcion");
                //String direccion = com.getString("direccion");
                //String telefono = com.getString("telefono");
                //cadena = cadena + id + "," + nomcomp + "," + nomarea + "\n";
                OperadorTuristico ot = new OperadorTuristico(nombresitio, tipo, descripcion, nombremunicipio, "");
                cs.add(ot);
            }
            Toast.makeText(getApplicationContext(),"Conexción completa", Toast.LENGTH_LONG).show();
            //dato.setText(cadena);
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}