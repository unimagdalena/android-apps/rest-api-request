package com.example.apirest;

import android.os.Parcel;
import android.os.Parcelable;

public class OperadorTuristico implements Parcelable {
    private final String nombre;
    private final String tipo;
    private final String descripcion;
    private final String nombremunicipio;
    private final String direccion;

    public OperadorTuristico(String nombre, String tipo, String descripcion, String nombremunicipio, String direccion) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.nombremunicipio = nombremunicipio;
        this.direccion = direccion;
    }

    protected OperadorTuristico(Parcel in) {
        nombre = in.readString();
        tipo = in.readString();
        descripcion = in.readString();
        nombremunicipio = in.readString();
        direccion = in.readString();
    }

    public static final Creator<OperadorTuristico> CREATOR = new Creator<OperadorTuristico>() {
        @Override
        public OperadorTuristico createFromParcel(Parcel in) {
            return new OperadorTuristico(in);
        }

        @Override
        public OperadorTuristico[] newArray(int size) {
            return new OperadorTuristico[size];
        }
    };

    public String getNombre() {
        return nombre;
    }


    public String getTipo() {
        return tipo;
    }


    public String getDescripcion() {
        return descripcion;
    }


    public String getNombremunicipio() {
        return nombremunicipio;
    }


    public String getDireccion() {
        return direccion;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nombre);
        parcel.writeString(tipo);
        parcel.writeString(descripcion);
        parcel.writeString(nombremunicipio);
        parcel.writeString(direccion);
    }
}
