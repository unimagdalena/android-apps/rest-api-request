package com.example.apirest;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class OperadorTuristicoAdapter extends ArrayAdapter<OperadorTuristico> {
    private List<OperadorTuristico> listitems;
    private Context context;
    private int resourceLayout;

    public OperadorTuristicoAdapter(@NonNull Context context, int resource, List<OperadorTuristico> objects) {
        super(context, resource,objects);
        this.listitems=objects;
        this.context=context;
        this.resourceLayout=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view =convertView;
        if(view==null){
            view=LayoutInflater.from(context).inflate(R.layout.item,null);
        }
        OperadorTuristico operador= listitems.get(position);

        TextView nombre=view.findViewById(R.id.nombre);
        TextView tipo=view.findViewById(R.id.tipo);
        TextView descripcion=view.findViewById(R.id.descripcion);
        TextView nombremunicipio=view.findViewById(R.id.nombremunicipio);
        TextView direccion=view.findViewById(R.id.direccion);
        nombre.setText(operador.getNombre());
        tipo.setText(operador.getTipo());
        descripcion.setText(operador.getDescripcion());
        nombremunicipio.setText(operador.getNombremunicipio());
        direccion.setText(operador.getDireccion());

        return view;
    }
}
